#ifndef BMP_ROTATOR_H
#define BMP_ROTATOR_H

#include "image.h"

void rotate_90( struct image* img, struct image* img_out );

#endif //BMP_ROTATOR_H
