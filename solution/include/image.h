#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>

#define PIXEL_STRUCT_SIZE 3
#define PIXEL_SIZE 4

struct pixel { uint8_t b, g, r; };

struct image {
	uint64_t width, height;
	struct pixel* data;
};

#endif //IMAGE_H
