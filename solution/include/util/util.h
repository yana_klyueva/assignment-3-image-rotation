#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>

void print_message(const char* message);

void print_error(const char* error);

#endif //UTIL_H
