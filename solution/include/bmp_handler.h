#ifndef FILE_HANDLER_H
#define FILE_HANDLER_H

#include "bmp_reader.h"
#include "bmp_writer.h"
#include "image_rotator.h"
#include <stdio.h>
#include <stdlib.h>

void bmp_rotate(char* in_file_path, char* out_file_path);

#endif
//FILE_HANDLER_H
