#ifndef BMP_READER_H
#define BMP_READER_H

#include "bmp_struct.h"
#include "util/util.h"
#include <malloc.h>
#include <stdio.h>

enum read_status  {
	READ_OK,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_FILE_IS_NULL,
	READ_PIXELS_ERROR
};

enum read_status from_bmp( FILE* in, struct image* img );
enum read_status read_header( struct bmp_header* header, uint64_t status );
enum read_status read_img( FILE* file, struct image* img, uint64_t status );

#endif //BMP_READER_H
