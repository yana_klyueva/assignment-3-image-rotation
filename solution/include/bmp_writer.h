#ifndef BMP_WRITER_H
#define BMP_WRITER_H

#include "bmp_struct.h"
#include "image.h"
#include "util/util.h"
#include <stdio.h>
#include <stdlib.h>

enum  write_status  {
	WRITE_OK,
	WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );

void fill_bmp_header(struct image img, struct bmp_header* header);

#endif //BMP_WRITER_H
