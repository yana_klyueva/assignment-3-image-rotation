#include "bmp_handler.h"

void bmp_rotate( char* in_file_path, char* out_file_path ) {
	FILE* file = fopen( in_file_path, "rb" );
	struct image* image = malloc( sizeof(struct image) );
	enum read_status status = from_bmp( file, image );
	fclose( file );
	
	if ( status == READ_FILE_IS_NULL ) 
		print_error( "пустой файл(" );
	if ( status == READ_INVALID_HEADER )
		print_error( "у файла некорректный заголовок" );
	if ( status == READ_INVALID_BITS ) 
		print_error( "файл содержит некорректные биты" );
	if ( status == READ_OK ) {
		file = fopen( out_file_path, "wb" );
		struct image* image_out = malloc( sizeof(struct image));
		rotate_90( image, image_out );
		free( image->data );
		free( image );	
		enum write_status write_status = to_bmp( file, image_out );
		if ( write_status == WRITE_ERROR )
			print_error( "ошибка во время записи в bmp формат" );
		fclose( file );
		free( image_out->data );
		free( image_out );
	}
}
