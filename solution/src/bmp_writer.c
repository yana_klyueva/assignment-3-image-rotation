#include "bmp_writer.h"

#define FILE_TYPE 19778
#define RESERVED 0
#define HEADERS_SIZE 54
#define INFO_HEADERS_SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define DEFAULT_X_PIXELS_PER_METER 2834
#define DEFAULT_Y_PIXELS_PER_METER 2834
#define ZERO_BY_DEFAULT 0

enum write_status to_bmp( FILE* out, struct image const* img ) {
	size_t size_bmp_header = sizeof( struct bmp_header );
	struct bmp_header* header =  malloc( size_bmp_header );
	fill_bmp_header( *img, header );
	uint64_t status = fwrite( header, size_bmp_header, 1, out );
	free( header );

	if( !status ) return WRITE_ERROR;

	int64_t padding = (int64_t)( PIXEL_SIZE - ((PIXEL_STRUCT_SIZE * img->width) % PIXEL_SIZE) ) % PIXEL_SIZE;
	size_t result;
	uint32_t helper = 0;

	for( uint32_t i = 0; i < img->height; i++ ) {
		for( uint32_t j = 0; j < img->width; j++) {
			status = fwrite( img->data + helper, sizeof(struct pixel), 1, out );
			if ( !status ) return WRITE_ERROR;
			helper++;
		}
		result = fseek( out, padding, SEEK_CUR );
		if ( result != 0 ) return WRITE_ERROR;
	}
	print_message( "данные были успешно записаны" );
	return WRITE_OK;
}

void fill_bmp_header( struct image const img, struct bmp_header* header ) {
	uint64_t padding = ( PIXEL_SIZE - ((PIXEL_STRUCT_SIZE * img.width) % PIXEL_SIZE) ) % PIXEL_SIZE;
	header->bfType = FILE_TYPE;
	header->bfileSize = img.width * img.height * PIXEL_STRUCT_SIZE + HEADERS_SIZE + img.height * padding;
	header->bfReserved = RESERVED;
	header->bOffBits = HEADERS_SIZE;
	header->biSize = INFO_HEADERS_SIZE;
	header->biWidth = img.width;
	header->biHeight = img.height;
	header->biPlanes = PLANES;
	header->biBitCount = BIT_COUNT;
	header->biCompression = COMPRESSION;
	header->biSizeImage = img.height * ( sizeof(struct pixel) * img.width + padding );
	header->biXPelsPerMeter = DEFAULT_X_PIXELS_PER_METER;
	header->biYPelsPerMeter = DEFAULT_Y_PIXELS_PER_METER;
	header->biClrUsed = ZERO_BY_DEFAULT;
	header->biClrImportant = ZERO_BY_DEFAULT;
}
