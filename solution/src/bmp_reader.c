#include "bmp_struct.h"
#include "bmp_reader.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

#define BIT_COUNT 24

enum read_status from_bmp( FILE* in, struct image* img ) {
	if( in == NULL ) return READ_FILE_IS_NULL;

	size_t size_bmp_header = sizeof(struct bmp_header);
	struct bmp_header* header = malloc( size_bmp_header );
	uint64_t status = fread( header, size_bmp_header, 1, in );

	enum read_status result = read_header( header, status );
	if( result != READ_OK ) {
		return result;
	}

	img->width = header->biWidth;
	img->height = header->biHeight;
	img->data = malloc( img->width * img->height * PIXEL_STRUCT_SIZE );
	free( header );

	result = read_img( in, img, status );
	return result;
}

enum read_status read_header( struct bmp_header* header, uint64_t status ) {
	if (!status) {
		free( header );
		return READ_INVALID_HEADER;
	}

	if (header->biBitCount != BIT_COUNT) {
		free( header );
		return READ_INVALID_BITS;
	}

	print_message( "заголовок был успешно считан" );
	return READ_OK;
}


enum read_status read_img( FILE* file, struct image* img, uint64_t status ) {
	int64_t padding = (int64_t)( PIXEL_SIZE - ((PIXEL_STRUCT_SIZE * img->width) % PIXEL_SIZE) ) % PIXEL_SIZE;
	uint32_t height = img->height;
	uint32_t width = img->width;
	size_t result;

	for( uint32_t i = 0; i < height; i++ ) {
		status = fread( img->data + i * width, PIXEL_STRUCT_SIZE, width, file);
		if ( status == 0 ) return READ_PIXELS_ERROR;
		result = fseek( file, padding, SEEK_CUR );
		if ( result != 0 ) return READ_INVALID_BITS;
	}
	
	print_message( "данные были успешно считаны)" );
	return READ_OK;
}
