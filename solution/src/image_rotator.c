#include "image_rotator.h"
#include "image.h"
#include <malloc.h>

void rotate_90( struct image* img, struct image* img_out ) {
	img_out->width = img->height;
	img_out->height = img->width;
	img_out->data =
 		malloc( img_out->width * img_out->height * PIXEL_STRUCT_SIZE);

	for (uint32_t i = 0; i < img->height; i++) {
		for (uint32_t j = 0; j < img->width; j++) {
			struct pixel pixel = img->data[img->width * i + j];
			img_out->data[img_out->width * j + (img->height - i - 1)] =
																		 pixel;
		}
	}
} 
