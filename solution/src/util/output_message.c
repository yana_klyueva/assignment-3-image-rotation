// output_message.c

#include "util/util.h"

void print_message( const char* const message ) {
	puts( message );
}

void print_error( const char* const error ) {
	fprintf( stderr, "%s\n", error );
}
