#include "bmp_handler.h"
#include <stdio.h>

int main( int argc, char** argv ) {
	if ( argc != 3 ) print_error( "количество аргументов должно быть 2!" );
	bmp_rotate( argv[1], argv[2] );
	return 0;
}
